package proj.com.inspection.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.jetbrains.anko.toast
import proj.com.inspection.R
import proj.com.inspection.models.InspectionResponse
import timber.log.Timber

class InspectionActivity : AppCompatActivity() {

    companion object {
        const val EXTRAS_INSPECTION = "extras-inspection"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inspection)

        if (intent == null || intent.getParcelableExtra<InspectionResponse>(EXTRAS_INSPECTION) == null) {
            throw Exception("Inspection Extras Not Found")
        }

        val inspectionItem = intent.getParcelableExtra<InspectionResponse>(EXTRAS_INSPECTION)
                ?: return

        Timber.i(inspectionItem.caseId?.toString() ?: "")
        toast("InspectionActivity started")

    }
}
