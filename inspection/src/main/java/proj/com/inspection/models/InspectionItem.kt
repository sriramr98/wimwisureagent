package proj.com.inspection.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class InspectionItem(
	val photoOrder: String? = null,
	val photoOptions: List<PhotoOptionsItem?>? = null,
	val voiceOverText: String? = null,
	val itemType: String? = null,
	val demoImage: Boolean? = null,
	val zipSuffix: String? = null,
	val name: String? = null,
	val multiple: Boolean? = null,
	val demoImageUrl: String? = null,
	val optional: Boolean? = null,
	val inputType: String? = null,
	val id: String? = null
): Parcelable
