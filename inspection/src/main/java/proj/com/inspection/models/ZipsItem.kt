package proj.com.inspection.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ZipsItem(
        val uploadOrder: Int? = null,
        val id: String? = null
) : Parcelable
