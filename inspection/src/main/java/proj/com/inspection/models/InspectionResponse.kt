package proj.com.inspection.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class InspectionResponse(
        val voiceOverLanguage: String? = null,
        val itemsList: List<InspectionItem?>? = null,
        val caseId: Int? = null,
        val zips: List<ZipsItem?>? = null,
        val imageQuality: Int? = null
) : Parcelable {

    companion object {
        @JvmStatic
        fun getDummyData(): InspectionResponse {
            return InspectionResponse("EN", arrayListOf(), 12, arrayListOf(), 80)
        }
    }

}
