package proj.com.inspection.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PhotoOptionsItem(
        val id: Int? = null,
        val option: String? = null
) : Parcelable
