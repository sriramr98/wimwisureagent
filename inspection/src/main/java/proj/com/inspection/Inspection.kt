package proj.com.inspection

import android.content.Context
import org.jetbrains.anko.startActivity
import proj.com.inspection.models.InspectionResponse
import proj.com.inspection.ui.InspectionActivity

fun Context.startInspection(inspection: InspectionResponse) {
    startActivity<InspectionActivity>(InspectionActivity.EXTRAS_INSPECTION to inspection)
}