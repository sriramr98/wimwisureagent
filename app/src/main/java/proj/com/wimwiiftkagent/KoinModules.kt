package proj.com.wimwiiftkagent

import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.applicationContext
import proj.com.wimwiiftkagent.network.*

val wimwisureModules = applicationContext {
    bean { getOkHttpInstance(get(), get()) }
    bean { getLoggingInterceptor() }
    bean { getCache(androidApplication()) }
    bean { getGsonFactory() }
    factory { getRetrofit(get(), get()) }
    bean { getAgentService(get()) }
}