package proj.com.wimwiiftkagent

import android.app.Application
import org.koin.android.ext.android.startKoin

class AgentApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, arrayListOf(wimwisureModules))
    }
}