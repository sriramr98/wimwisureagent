package proj.com.wimwiiftkagent.network

import android.content.Context
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import proj.com.wimwiiftkagent.utils.Constants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.io.File

fun getOkHttpInstance(loggingInterceptor: HttpLoggingInterceptor, cache: Cache): OkHttpClient {
    return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .cache(cache)
            .build()
}

fun getLoggingInterceptor(): HttpLoggingInterceptor {
    val loggingInterceptor = HttpLoggingInterceptor { message -> Timber.i(message) }

    loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    return loggingInterceptor
}

fun getCache(context: Context): Cache {
    val cacheFile = File(context.cacheDir, "okhttp-cache")

    return Cache(cacheFile, 35 * 1000 * 1000)
}

fun getRetrofit(okHttpClient: OkHttpClient, gsonConverterFactory: GsonConverterFactory): Retrofit {
    return Retrofit.Builder()
            .addConverterFactory(gsonConverterFactory)
            .baseUrl(Constants.API_BASE_URL)
            .client(okHttpClient)
            .build()
}

fun getGsonFactory(): GsonConverterFactory {
    return GsonConverterFactory.create()
}


fun getAgentService(retrofit: Retrofit): AgentService {
    return retrofit.create(AgentService::class.java)
}